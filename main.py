import os
import random

import time

from constant import *
import pyhdb


def is_rpi():
    return 'arm' in os.uname()[4]


if is_rpi():
    import Adafruit_DHT


def read_dht():
    if is_rpi():
        sensor = Adafruit_DHT.DHT22
        humidity, temperature = Adafruit_DHT.read_retry(sensor, DHT_PIN)

        if humidity is not None and temperature is not None:
            print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
            return int(humidity), int(temperature)
        else:
            return None, None
    else:
        return random.randint(20, 30), random.randint(40, 70)


if __name__ == '__main__':

    connection = pyhdb.connect(host=SAP_HOST, port=30015, user=SAP_USER, password=SAP_PWD)
    cursor = connection.cursor()

    while True:
        s_time = time.time()
        cursor.execute('SELECT COUNT(*) from "{}"."{}";'.format(SAP_SCHEMA,SAP_TABLE))
        row_count = cursor.fetchone()[0]

        temp, humi = read_dht()
        query = "INSERT INTO \"{}\".\"{}\" VALUES({}, '{}', {}, {}, '{}')".format(
            SAP_SCHEMA, SAP_TABLE, row_count + 1, DEVICE_ID, temp, humi, ROOM_NAME)
        print('Executing query: ', query)
        cursor.execute(query)

        connection.commit()

        cursor.execute('SELECT COUNT(*) from "{}"."{}";'.format(SAP_SCHEMA, SAP_TABLE))
        row_count = cursor.fetchone()[0]
        print('New Row Count: ', row_count)
        elapsed = time.time() - s_time
        time.sleep(max(0, 5 - elapsed))

    connection.close()
